This program first trains a LSI-TFIDF model on the corpus, then ranks documents in decreasing order of query-document cosine similarities.

Instructions:
1. python setup.py install
2. open terminal -> python, 
import nltk
nltk.download('all')
3. python docs_similarity.py corpus_coding_test.tsv
   then wait until the corpus preprocessing done (around 60s)
   follow the instructions from the terminal

SVD(Singular Value Decomposition):

The U matrix gives us the coordinates of each word on our “concept” space, 
the S matrix of singular values gives us a clue as to how many dimensions or “concepts” we need to include,
and the Vt matrix gives us the coordinates of each document in our “concept” space.

The left singular vectors are stored in lsi.projection.u, 
singular values in lsi.projection.s. 
Right singular vectors can be reconstructed from the output of lsi[training_corpus]