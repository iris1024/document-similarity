# -*- coding: utf-8 -*-
#!/usr/bin/env python
__author__ = "Zili Chen"

import os, sys
import argparse
import nltk
import HTMLParser
h = HTMLParser.HTMLParser()
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.lancaster import LancasterStemmer
st = LancasterStemmer()
import unicodedata
import re
from collections import OrderedDict
from gensim import corpora, models, similarities
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def line_preprocessing(line):
	"""Preprocess the corpus

	@para line: one raw line of the tsv
	@rtype content_stemmed: preprocessed line
	"""
	# T&ocirc;i n&oacute;i, -> Tôi nói, -> Toi noi,
	content = unicodedata.normalize('NFKD', h.unescape(line.lower().decode('utf8'))).encode('ascii','ignore')
	# remove any other &www; (html character code) that cannot be converted by pervious step
	content = re.sub(r'&\w+;',' ', content)
	# remove ~*~*~*~*~*sorry, ... ,  you.me -> you me
	content_misc_removed = re.sub(r'[-_*~]+|\.{2,}', ' ', content)
	content_misc_removed = re.sub(r'(\w*)(\.)(\w+)', r'\1 \3', content_misc_removed)
	# tokenize words
	content_tokenized = [word for word in word_tokenize(content_misc_removed)]
	# load english stopwords, remove them from the word list
	english_stopwords = stopwords.words('english')
	content_stopwords_removed = [word for word in content_tokenized if not word in english_stopwords]
	# remove punctuations from the word list(may generated by tokenize, hello! -> hello !)
	punctuations = [',', '.',':', ';', '?', '!','(', ')', '[', ']','{', '}', '&','*', '@', '#', '$','%','`','|','``','\'','"','\'\'','=','>','<']
	content_punctuations_removed = [word for word in content_stopwords_removed if not word in punctuations]
	# remove morphological affixes from words, eg: crying -> cry
	content_stemmed = [st.stem(word) for word in content_punctuations_removed]

	return content_stemmed

def corpus_preprocessing(line, corpus):
	"""Preprocess the corpus

	@para line: each line of the tsv
	@para corpus: stores the preprocessed data, eg: {index: ['word1','word2']}
	@rtype None
	"""
	# get index and document from the line
	[index, content] = line.strip().split('\t')
	content_stemmed = line_preprocessing(content)
	# build the corpus
	corpus[index] = content_stemmed

def query(corpus, docs_dict, line, lsi, index, k):
	"""Execute a query on lsi model

	@para corpus: stores the preprocessed data, eg: {index: ['word1','word2']}
	@para docs_dict: corpora.dictionary.Dictionary object, eg: Dictionary(11 unique tokens: [u'a', u'damaged']...)
	@para line: input query
	@para lsi: lsimodel previous bulit
	@para index: index of lsi model
	@para k: the number of results need 
	@rtype None
	"""
	# spilt the docid and content from a new query, and do the same preprocessing
	[docid, content] = line.strip().split('\t')
	doc = line_preprocessing(content)
	# convert tokenized query to sparse vectors
	bow = docs_dict.doc2bow(doc)
	# reconstruct Vt matrix, which gives us the coordinates of each document in our “concept” space
	my_lsi = lsi[bow]
	# Rank documents in decreasing order of query-document cosine similarities.
	sims = index[my_lsi]
	sort_sims = sorted(enumerate(sims), key=lambda x: -x[1])
	print docid,'\t',
	for i in range(k):
		print '{0}:{1}, '.format(corpus.keys()[sort_sims[i][0]], sort_sims[i][1]),
	print '\n'

def list_all_corpus(corpus, docs_dict, lsi, index, k):
	"""Display all the top k similar docs of each doc in the corpus

	@para corpus: stores the preprocessed data, eg: {index: ['word1','word2']}
	@para docs_dict: corpora.dictionary.Dictionary object, eg: Dictionary(11 unique tokens: [u'a', u'damaged']...)
	@para lsi: lsimodel previous bulit
	@para index: index of lsi model
	@para k: the number of results need 
	@rtype None
	"""
	for doc in corpus.items():
		# almost the same as query function, just different on docid
		bow = docs_dict.doc2bow(doc[1])
		my_lsi = lsi[bow]
		sims = index[my_lsi]
		sort_sims = sorted(enumerate(sims), key=lambda x: -x[1])
		print doc[0],'	',
		for i in range(k):
			print '{0}:{1}, '.format(corpus.keys()[sort_sims[i][0]], sort_sims[i][1]),
		print '\n'

def LSI(corpus):
	"""Build tfidf and lsi model based on the corpus,
	   query the similarity of the query vector against every document in the corpus.

	@para corpus: stores the preprocessed data, eg: {index: ['word1','word2']}
	@rtype None
	"""
	# use bag-of-words to convert documents to vectors, mapping each token in the corpus to an id
	docs_dict = corpora.Dictionary(corpus.values())
	# remove words that appear only once
	once_ids = [tokenid for tokenid, docfreq in docs_dict.dfs.iteritems() if docfreq == 1]
	docs_dict.filter_tokens(once_ids) 
	# remove gaps in id sequence after words that were removed
	docs_dict.compactify()
	# convert tokenized documents to sparse vectors
	docs_corpus = [docs_dict.doc2bow(doc) for doc in corpus.values()]
	# initialize a transformation, convert one vector representation into another -- tfidf model
	tfidf = models.TfidfModel(docs_corpus)
	# transform the whole corpus via TFIDF
	docs_tfidf = tfidf[docs_corpus]
	# train lsi model based on the corpus transformed from TFIDF
	lsi = models.LsiModel(docs_tfidf, id2word = docs_dict, num_topics=100)
	# build index, in preparation for similarity queries
	index = similarities.MatrixSimilarity(lsi[docs_corpus])

	while 1:
		option = raw_input("\nYou have 3 options, Enter \n"
			"1) query on a doc with id and content,\n"
			"2) list results for each doc in the corpus,\n"
			"or Enter quit to quit. \n")
		
		if option == 'quit':
			sys.stderr.write("Quiting\n")
			sys.exit()

		elif option == '1':
			raw_query = raw_input("\nEnter the query: \n")
			if (raw_query == ''):
				sys.stderr.write("Empty query\n")
				continue
			k = raw_input("\nEnter the number of k for top k similar documents: \n")
			if (k.isdigit() == False):
				sys.stderr.write("Not a digit, default k = 5.\n")
				k = '5'
			query(corpus, docs_dict, raw_query, lsi, index, int(k))

		elif option == '2':
			k = raw_input("\nEnter the number of k for top k similar documents: \n")
			if (k.isdigit() == False):
				sys.stderr.write("Not a digit, default k = 5.\n")
				k = '5'
			list_all_corpus(corpus, docs_dict, lsi, index, int(k))

if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument('corpusDir', type=str, help='directory of corpus_coding_test.tsv')
	args = parser.parse_args()

	# file path check
	if (os.path.exists(args.corpusDir) == False):
		sys.stderr.write("File not found.")
		sys.exit()

	sys.stderr.write("Preprocessing corpus start.\n")

	# read file and preprocessing
	tsvFile = open(args.corpusDir, 'r')
	corpus = OrderedDict()
	for line in tsvFile.readlines():
		# skip blank line
		if (line.strip() == ''):
			continue
		corpus_preprocessing(line, corpus)
	sys.stderr.write("Preprocessing corpus done.\n")

	# build LSI model
	LSI(corpus)