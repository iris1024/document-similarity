from setuptools import setup, find_packages

setup(
    name = "Document_Similarity",
    version = "1.0",
    packages = find_packages(),
    scripts = ['docs_similarity.py'],
    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires = [
        "nltk",
        "gensim"
    ],
)